const express = require('express');
const routes = require('./routes');

const app = express();

routes.setupRoutes(app);

module.exports = app;
